# Dib2

Collaborative concept mapping.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version (or, but only for the 'dibdib.all'
    parts of the source code, the matching LGPL variant of GNU).

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

Work in progress: source files to be re-used.
Cmp. other project files under gitlab.com/dibdib, gitlab.com/gxworks, and dibdib.sf.net.

(Impressum: https://gxworks.github.io/impressum)
